## CoLab Applications(shiftr, colab website, discover, bluesmith, pulse) 
* Matt Gatner - matt.gatner@duke.edu
* Anni Yan - anni.yan@duke.edu


## Student Developers/Office Hours 
* Anna Mollard (lead student dev) - acm105@duke.edu
* Anni Yan - anni.yan@duke.edu


## Roots Program 
* Sandra Bermond - sandra.bermond@duke.edu


## VCM Virtual Machines and/or Course Containers 
Please do not wait until the week and/or day of your class to request VMs and/or containers. 
We are going to be short staffed compounded by the fact that we will have a short turnaround after the holidays.
If you know you're going to need these resources, please have them requested before the semester ends.

* New/Renewal Requests: https://oit.duke.edu/what-we-do/applications/custom-software-environments-duke-courses
* Troubleshooting VMs/Containers - OIT Service Desk


## OAuth Integrations 
* student integrations: Matt Gatner - matt.gatner@duke.edu
* any staff requests and faculty requests(not course related) should go to the **OIT service desk**.


## Academic Parterships, CCT, and anything not covered 
* Michael Faber - maf13@duke.edu
